# Nos Racines / Noastras Raïç (occitan)

Solution de gestion de cimetière simple et libre:

![Image de présentation de Nos Racines](https://framagit.org/fractale-abiterra/nos-racines/raw/master/ressources/img.png)

Le projet est sous licence CC-BY-SA 4.0

mention à inscrire lors de l'utilisation de ces informations:

Pour le projet Qgis
Fractale - francois raynaud - Michel HamzaoUi




## Version de Qgis
ce projet nécessite l'installation du logiciel Qgis en version 2. Il n'a pas été configuré pour les versions 3 pour l'instant.
Guide vidéo ajout de concession: https://vimeo.com/241027347


## Version Lizmap
Fractale propose une version web de cette solution basée sur lizmap. 

Pour tout renseignements

contact@fractale.xyz

https://fractale.xyz


## À propos de Fractale

Installés à Gentioux-Pigerolles,
nous sommes deux cartographes
diplômés de master de géographie
à Rennes et Paris. Nous avons choisi
d’habiter la montagne limousine dans l’idée
d’apporter nos compétences de cartographie à ce territoire.


